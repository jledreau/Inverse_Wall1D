# -*- coding: utf-8 -*-
import json
import os
import sys

import numpy as np
import pandas as pd


class NUM_data:
    """

    """

    def __init__(self, file, folder, wall, index):
        """

        """

        self.num_lgd = file["num_lgd"]  # how to print variables ["label", "color", linestyle", "dotstyle"]
        self.data_num_raw = pd.read_csv(folder + os.sep + file["source file"], delimiter='\t', skiprows=4)
        self.data_num_raw.set_index(file["col_time"], inplace=True)
        for col in self.data_num_raw.columns:

            self.data_num_raw.rename(columns={col: self.num_lgd[[key for key in self.num_lgd.keys() if key == col][0]][0]},
                                inplace=True)
        self.name_sensor_T12 = "T1-2_num"
        if file["Unit"] == "K" :
            self.data_num_raw -=273.15

        self.data_num_raw.index = index
        self.data_num_raw.round(3)

        # to csv
        self.data_num_raw.to_csv(folder + os.sep + "postprocessing" + os.sep + "data_num_cleaned.csv", sep=';')


def init(choosen_file, wall, index):
    """
    """
    folder = os.path.split(choosen_file)[0]
    # file = os.path.basename(choosen_file).split('/')[-1]
    # num_data = NUM_data(file, folder, wall, xp_data, unit, col_time)
    with open(choosen_file) as json_name:
        file = json.load(json_name)
        num_data = NUM_data(file, folder, wall, index)
    return num_data
