# -*- coding: utf-8 -*-
import json
import os

class Wall:
    """
    wall class describes ONLY FOR 3 LAYER WALLS with 5 sensors (Outside / Layer 1 / Layer 2 / Layer 3 / Inside)
    if need less, split a layer into 2.

    """

    def __init__(self, file, folder):
        """
        geometrical and thermal properties of wall
        import dthermal conductivityata from file.json (example given in Walls folder)

        L1, L2, L3 : Thermal conductivity of layer 1, 2 and 3 (in W/m/K)
        C1, C2, C3 : Volumetric heat capacity of layer 1, 2 and 3 (in J/m3/K)
        e1, e2, e3 : Thickness of layer 1, 2 and 3 (in m)
        L : Total Thickness of wall (in m)
        S1, S2, S3, S4, S5 : Location of sensors from outside (outside surface =0, inside surface = L) (in m)
        """
        self.L1 = file["thermal conductivity"]["layer1"]  # [W/m/K]
        self.L2 = file["thermal conductivity"]["layer2"]  # [W/m/K]
        self.L3 = file["thermal conductivity"]["layer3"]  # [W/m/K]
        self.C1 = file["volumetric heat capacity"]["layer1"]  # [J/m3/K]
        self.C2 = file["volumetric heat capacity"]["layer2"]  # [J/m3/K]
        self.C3 = file["volumetric heat capacity"]["layer3"]  # [J/m3/K]
        self.e1 = file["thickness"]["layer1"]  # [m]
        self.e2 = file["thickness"]["layer2"]  # [m]
        self.e3 = file["thickness"]["layer3"]  # [m]
        self.L = self.e1 + self.e2 + self.e3
        self.S1 = file["sensor location"]["sensor1"]  # [m]
        self.S2 = file["sensor location"]["sensor2"]  # [m]
        self.S3 = file["sensor location"]["sensor3"]  # [m]
        self.S4 = file["sensor location"]["sensor4"]  # [m]
        self.S5 = file["sensor location"]["sensor5"]  # [m]
        self.abSW_out = file["radiative properties"]["SW absorptivity"]
        self.abLW_out = file["radiative properties"]["LW emissivity out"]
        self.abLW_in = file["radiative properties"]["LW emissivity in"]
        self.u_abSW_out = file["radiative properties"]["u_SW absorptivity"]
        self.u_abLW_out = file["radiative properties"]["u_LW emissivity out"]
        self.u_abLW_in = file["radiative properties"]["u_LW emissivity in"]

        self.symmetry = file["symmetry"]

        #constantes
        self.sigma = 5.67E-8

def init(choosen_file):
    """
    """
    folder = os.path.split(choosen_file)[0]
    with open(choosen_file) as json_name:
        file = json.load(json_name)
        wall = Wall(file, folder)
    return wall
