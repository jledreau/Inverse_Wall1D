# -*- coding: utf-8 -*-
import json
import os

import numpy as np
import pandas as pd


class XP_data:
    """

    """

    def __init__(self, file, folder, wall):
        """

        """

        data_xp_raw = pd.read_csv(folder + os.sep + file["source file"], delimiter=';')
        data_xp_raw['Horodatage'] = pd.to_datetime(data_xp_raw['Horodatage'], format='%d/%m/%Y %H:%M')
        data_xp_raw = data_xp_raw.set_index('Horodatage')

        self.data_xp = pd.DataFrame(index=data_xp_raw.index)

        self.time_min, self.time_max = self.data_xp.index.min(), self.data_xp.index.max()
        self.step = ((self.time_max - self.time_min) / (self.data_xp.shape[0] - 1)).seconds

        self.data_xp["Tair_out"] = data_xp_raw[file["outside BC"]["Tair_out"]].mean(axis=1).round(3)  # °C
        self.data_xp["P_SW"] = data_xp_raw[file["outside BC"]["P_SW"]].mean(axis=1).round(0)  # W/m²
        self.data_xp["T_b"] = ((data_xp_raw[file["outside BC"]["P_LW_net"]].mean(axis=1)/
                                wall.sigma + data_xp_raw[file["outside BC"]["T_sensor_LW"]].mean(
                    axis=1)**4)  ** (1 / 4) - 273.15).round(3)  # °C
        self.data_xp["P_LW"] = (data_xp_raw[file["outside BC"]["P_LW_net"]].mean(axis=1) + wall.sigma *
                                data_xp_raw[file["outside BC"]["T_sensor_LW"]].mean(axis=1) **4).round(3)  # W/m2

        self.data_xp["Tair_in"] = data_xp_raw[file["inside BC"]["Tair_in"]].mean(axis=1).round(3)  # °C
        self.data_xp["TLW_in"] = data_xp_raw[file["inside BC"]["Tsurf_in"]].mean(axis=1).round(3)  # °C

        self.data_xp["wind_speed"] = data_xp_raw[file["wind"]["speed"]].mean(axis=1).round(3)  # m/s
        self.data_xp["wind_dir"] = data_xp_raw[file["wind"]["dir"]].mean(axis=1).round(0)  # °

        self.data_xp["Tsout"] = data_xp_raw[file["inside wall"]["S1"]].mean(axis=1).round(3)  # °C
        self.data_xp["T1-2"] = data_xp_raw[file["inside wall"]["S2"]].mean(axis=1).round(3)  # °C
        self.data_xp["T2"] = data_xp_raw[file["inside wall"]["S3"]].mean(axis=1).round(3)  # °C
        self.data_xp["T2-3"] = data_xp_raw[file["inside wall"]["S4"]].mean(axis=1).round(3)  # °C
        self.data_xp["Tsin"] = data_xp_raw[file["inside wall"]["S5"]].mean(axis=1).round(3)  # °C

        dat = pd.DataFrame(index=self.data_xp.index)
        dat['mean'] = 2.5
        dat['st'] = 0.25
        self.data_xp["hconv_in"] = (np.random.normal(dat['mean'], dat['st'])).round(3)
        self.data_xp["hr_in"] = (4 * wall.sigma * wall.abLW_in * (
                self.data_xp[["Tsin", "TLW_in"]].mean(axis=1) + 273.15) ** 3).round(3)
        self.data_xp["h_in"] = self.data_xp["hconv_in"] + self.data_xp["hr_in"]

        self.data_xp["T_in"] = ((self.data_xp["hr_in"] * self.data_xp["Tsin"] + self.data_xp["hconv_in"] *
                                self.data_xp["Tair_in"]) / self.data_xp["h_in"]).round(3)

        self.data_xp["hr_out"] = 4 * wall.sigma * wall.abLW_out * (self.data_xp[["Tsout", "T_b"]].mean(axis=1)+273.15) ** 3

        height_sensors = 2.5 #m
        height_weather_station = 15 #Orme 1999 - effective leakage area model  - flat terrain with some isolated obstacles
        Conv_Uwind = (height_sensors/height_weather_station)**0.15
        self.data_xp["wind_speed_2.5m"] = Conv_Uwind * self.data_xp["wind_speed"]

        self.data_xp["hconv_out_ISO6946"] = (4 * self.data_xp["wind_speed_2.5m"] + 4).round(3)
        self.data_xp['hconvout_const'] = 4 + 4 * 4 * Conv_Uwind  # ISO 6946 with constant

        def is_between(a, x, b):
            return min(a, b) < x < max(a, b)

        self.data_xp['hconvout_Liu'] = [1.53 * x + 1.43 if is_between(108, y, 288) else 0.9 * x + 3.28 for
                                        (x, y) in
                                        zip(self.data_xp["wind_speed"],
                                            self.data_xp["wind_dir"])]
        self.data_xp['hconvout_Liu'] = (self.data_xp['hconvout_Liu']).round(3)

        conditions = [
            (self.data_xp["wind_speed"] <= 2) & (self.data_xp["wind_dir"] <= 288) & (self.data_xp["wind_dir"] >= 108),
            (self.data_xp["wind_speed"] > 2) & (self.data_xp["wind_dir"] <= 288) & (self.data_xp["wind_dir"] >= 108),
            (self.data_xp["wind_dir"] < 108) | (self.data_xp["wind_dir"] > 288)]

        choices = [18.6 * 0.5 ** 0.605, 18.6 * (0.25 * self.data_xp["wind_speed"]) ** 0.605, 18.6 * (0.05 * self.data_xp["wind_speed"] + 0.3) ** 0.605]
        self.data_xp['hconvout_ahsrae'] = np.select(conditions, choices, default=np.nan)

        self.data_xp['hconvout_ahsrae'] = (self.data_xp['hconvout_ahsrae']).round(3)

        self.xp_lgd = file["xp_lgd"] # how to print variables ["label", "color", linestyle", "dotstyle"]

        # to csv
        pd.to_datetime(self.data_xp.index, format='%d-%m-%Y %H:%M:%S')
        self.data_xp.to_csv(folder + os.sep + "postprocessing"+os.sep+"data_xp_cleaned.csv", sep=';')

def init(choosen_file, wall):
    """
    """
    folder = os.path.split(choosen_file)[0]
    with open(choosen_file) as json_name:
        file = json.load(json_name)
        xp_data = XP_data(file, folder, wall)
    return xp_data