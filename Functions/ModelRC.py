# -*- coding: utf-8 -*-

import os

import numpy as np
import pandas as pd


def ModelRC_BC1(Modeling, nb13, nb2, hcout, list_sensors, file, folder) -> pd.DataFrame():
    """ModelRC contains ModelRC function of type (nb13*2+nb2+3)R(nb13*2+nb2)C
    Describes heat transfer with a number of R and C you chose
    ONLY FOR 3 LAYER WALLS (Outside / Layer 1 / Layer 2 / Layer 3 / Inside)
    Model of type (nb+3)R(nb)C

    Parameters:
    Ge         : Outside heat transfert coef for conv (in W/m²/K), array
    C0         : Type of BC (if np.zeros (5) -> steady state conditions, else, values of Tini for the 5 nodes of interest)
    sensor     : if 1, compute 5 nodes of interest, if 0 compute sensors T2 and T23

    Return: matrix of temperature of 5 points (Text, T12, T2, T23 and Tin)
    """
    # ------------------------------------------------------------------------------------------------------------------
    # ---------------------------------CALCULATION OF NODES NUMBER------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------
    nb = int(nb2 + nb13 * 2 + 4)  # Total number of nodes (including surfaces and interfaces = +4)
    node_inf, node_sup = int(nb13 + 1 + (nb2 + 1) / 2 * (1 + Modeling.pos)), int(
        nb13 + 1 + (nb2 + 1) / 2 * (
                1 + Modeling.pos)) + 1  # node before and node after exact location
    x_inf, x_sup, x_pos = (node_inf - nb13 - 1) / (nb2 + 1) * 120, (node_sup - nb13 - 1) / (
            nb2 + 1) * 120, (
                                  1 + Modeling.pos) * 60  # location in m of node before, node after and exact location

    # ------------------------------------------------------------------------------------------------------------------
    # ---------------------------------------MATRICES C, A and B--------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------
    S = 1  # Surface

    # heat transfer coeffients
    hin_mean = Modeling.xp_data.data_xp['hr_in'].mean() + Modeling.xp_data.data_xp['hconv_in'].mean()
    hcout_series = Modeling.xp_data.data_xp[hcout]

    # BC
    Te = Modeling.xp_data.data_xp['Tair_out']
    Tb = Modeling.xp_data.data_xp['T_b']
    Tin = Modeling.xp_data.data_xp['T_in']
    PSW = Modeling.xp_data.data_xp['P_SW']

    G1, G2, G3 = Modeling.L1 * S / Modeling.e1, Modeling.L2 * S / Modeling.e2, Modeling.L3 * S / Modeling.e3  # Conductance of layer 1, 2 and 3
    G1_n, G2_n, G3_n = G1 * (nb13 + 1), G2 * (nb2 + 1), G3 * (
            nb13 + 1)  # Conductance of layer 1, 2 and 3 for each slice
    C1_n, C2_n, C3_n = Modeling.C1 / nb13 * Modeling.e1, Modeling.C2 / nb2 * Modeling.e2, \
                       Modeling.C3 / nb13 * Modeling.e3  # Capacity of layer 1, 2 and 3 for each slice

    # Matrix C : Capacities
    # capacity equals 0 for nodes on surfaces and interfaces
    C = np.zeros((nb, nb))
    for i in range(1, nb13 + 1):
        C[i, i] = C1_n
    for i in range(nb13 + 2, nb13 + nb2 + 2):
        C[i, i] = C2_n
    for i in range(nb13 + nb2 + 3, nb - 1):
        C[i, i] = C3_n

    # Matrix A : matrix of conductances
    A = np.zeros((nb, nb))
    A[0, 1] = G1_n
    for i in range(1, nb13 + 1):
        A[i, i - 1:i + 2] = np.array([G1_n, -2 * G1_n, G1_n])
    for i in range(nb13 + 2, nb2 + nb13 + 2):
        A[i, i - 1:i + 2] = np.array([G2_n, -2 * G2_n, G2_n])
    for i in range(nb2 + nb13 - 1, nb - 1):
        A[i, i - 1:i + 2] = np.array([G3_n, -2 * G3_n, G3_n])
    A[nb13 + 1, nb13:nb13 + 3] = np.array([G1_n, -G1_n - G2_n, G2_n])
    A[nb2 + nb13 + 2, nb2 + nb13 + 1:nb2 + nb13 + 4] = np.array([G2_n, -G2_n - G3_n, G3_n])
    A[-1, -2:] = np.array([G3_n, -G3_n - hin_mean])

    # Vector B : source
    B = np.zeros(nb)
    B[-1] = hin_mean * Tin[0]
    # ------------------------------------------------------------------------------------------------------------------
    # -----------------------------------SOLVING -----------------------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------
    Sol = np.zeros((Modeling.numstep, 5))  # matrix of solution

    # Solving
    Z = np.zeros((nb, nb))
    W = np.zeros((nb, nb))
    T = np.zeros(nb)
    V = np.zeros(nb)

    # Initial values
    T[:] = CI_RC_BC1(Modeling, A, B, C, nb, hcout_series[0], G1_n, Tb[0], Te[0], PSW[0])

    Sol[0, :] = np.array(
        [T[0], T[nb13 + 1], (T[node_sup] - T[node_inf]) * (x_pos - x_inf) / (x_sup - x_inf) + T[node_inf],
         T[nb13 + nb2 + 2], T[-1]])

    # for each time step
    for i in range(1, Modeling.numstep):
        B[-1] = hin_mean * Tin[i]
        in_ = 1.
        out_ = 0.
        Test = Sol[i - 1, 0]
        while (np.abs(in_ - out_) > Modeling.epsilon):  # check heat balance outside
            G_LW = Modeling.ab_LW_out * 4 * Modeling.sigma * (
                        (Test + Tb[i] + 273.15 * 2) / 2) ** 3  # heat transfer coef for rad outside
            Ge_cr = hcout_series[i] + G_LW  # heat transfer coef for conv + rad outside
            Teq = (hcout_series[i] * Te[i] + G_LW * Tb[i] + Modeling.ab_SW * PSW[i]) / Ge_cr  # eq temperature outside
            A[0, 0] = -G1_n - Ge_cr
            B[0] = Ge_cr * Teq
            Z = C / Modeling.step - A
            W = C / Modeling.step
            V = np.add(np.dot(W, T), B)
            T = np.linalg.solve(Z, V)
            in_ = Ge_cr * (Teq - T[0])
            out_ = G1_n * (T[0] - T[1])
            Test = T[0]
        Sol[i, :] = np.array(
            [T[0], T[nb13 + 1], (T[node_sup] - T[node_inf]) * (x_pos - x_inf) / (x_sup - x_inf) + T[node_inf],
             T[nb13 + nb2 + 2], T[-1]])
    result = pd.DataFrame(Sol, index=Modeling.xp_data.data_xp.index, columns=list_sensors)
    result = result.round(3)
    if file !='':
        result.to_csv(folder + os.sep + file + '_BC1_RC' + str(nb13) + str(nb2) + '.csv', sep=';')

    return result


def CI_RC_BC1(Modeling, A, B, C, nb, hcout0, G1_n, Tb, Te, PSW):
    """CI for initial temperature for
    ONLY FOR 3 LAYER WALLS (Outside / Layer 1 / Layer 2 / Layer 3 / Inside)
    Parameters:
    nb         : Total number of nodes
    A          : Matrix of conductances
    B          : Source
    C          : Matrix of capacities

    Return: vector of nb initial values
    """
    step = 10 * 24 * 3600.
    epsilon = 1e-5

    Temp0, T_1 = np.zeros(nb), np.ones(nb) * 100
    Test = Te

    while (np.abs(np.amax(Temp0 - T_1)) > epsilon):
        T_1 = Temp0
        G_LW = Modeling.ab_LW_out * 4 * Modeling.sigma * (
                (Test + Tb + 273.15 * 2) / 2) ** 3  # heat transfer coef for rad outside
        Ge_cr = hcout0 + G_LW  # heat transfer coef for conv + rad outside
        Teq = (hcout0 * Te + G_LW * Tb + Modeling.ab_SW * PSW) / Ge_cr  # eq temperature outside
        A[0, 0] = -G1_n - Ge_cr
        B[0] = Ge_cr * Teq
        Z = C / step - A
        W = C / step
        V = np.add(np.dot(W, Temp0), B)
        Temp0 = np.linalg.solve(Z, V)
        Test = Temp0[0]

    return Temp0


def ModelRC_BC2(Modeling, nb13, nb2, BC_type, list_sensors, file, folder):
    """ModelRC contains ModelRC function of type (nb13*2+nb2+3)R(nb13*2+nb2)C
    Describes heat transfer with a number of R and C you chose
    ONLY FOR 3 LAYER WALLS (Outside / Layer 1 / Layer 2 / Layer 3 / Inside)
    Model of type (nb+3)R(nb)C

    Parameters:&
    L1, L2, L3 : Thermal conductivity of layer 1, 2 and 3 (in W/m/K)
    C1, C2, C3 : Volumetric heat capacity of layer 1, 2 and 3 (in J/m3/K)
    e1, e2, e3 : Thickness of layer 1, 2 and 3 (in m)
    st         : Start time of simulation (in s)
    ed         : End of simulation (in s)
    step       : Step of simulation (in s)
    nb2        : Number of nodes with capacity in layer 2
    nb13       : Number of nodes with capacity in layer 1 ( = number in layer 3)
    abSW       : Solar absoptivity of the wall, double
    abLW       : LW absorptivity of the wall, double
    Ge         : Outside heat transfert coef for conv (in W/m²/K), array
    Gi         : Inside utside heat transfert coef for conv and rad (in W/m²/K), array but considered as scalar (Gi.mean())
    Te         : Outside air temperature (in degC), array
    Ti         : Inside operative temperature (in degC), array
    Tb         : Outside brightness temperature (in degC), array
    Psol       : Vertical solar radiation on wall (in W/m²), array
    C0         : Type of BC (if np.zeros (5) -> steady state conditions, else, values of Tini for the 5 nodes of interest)
    pos        : location of sensor T2 = if 0, middle of the layer, if 1, between layer 2 and 3
    epsilon    : accuracy of the model, difference between heatflux in and heatflux out at node out, W/m²
    sensors    : if 1, compute 5 nodes of interest, if 0 compute sensors T2 and T23

    Return: matrix of temperature of 5 points (Text, T12, T2, T23 and Tin)
    """
    # ------------------------------------------------------------------------------------------------------------------
    # ---------------------------------CALCULATION OF NODES NUMBER------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------
    nb = int(nb2 + nb13 + 2)  # Total number of nodes (including surfaces and interfaces = +2)
    # or sensor T2, if pos =/0
    node_inf, node_sup = int(nb2 / 2 * (1 + Modeling.pos)), int(
        nb2 / 2 * (1 + Modeling.pos)) + 1  # node before and node after exact location
    x_inf, x_sup, x_pos = (node_inf + 1) / (nb2 + 1) * 120, (node_sup + 1) / (nb2 + 1) * 120, (
            1 + Modeling.pos) * 60  # location in m of node before, node

    # ------------------------------------------------------------------------------------------------------------------
    # ---------------------------------------MATRICES C, A and B--------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------
    # heat transfer coeffients
    hin_mean = Modeling.xp_data.data_xp['hr_in'].mean() + Modeling.xp_data.data_xp['hconv_in'].mean()

    # BC
    if BC_type == 'num':
        T12 = Modeling.num_data.data_num_raw[Modeling.num_data.name_sensor_T12]
    elif BC_type == 'xp':
        T12 = Modeling.xp_data.data_xp['T1-2']
    Tin = Modeling.xp_data.data_xp['T_in']
    S = 1  # Surface

    G2, G3 = Modeling.L2 * S / Modeling.e2, Modeling.L3 * S / Modeling.e3  # Conductance of layer 2 and 3
    G2_n, G3_n = G2 * (nb2 + 1), G3 * (nb13 + 1)  # Conductance of layer 2 and 3 for each slice
    C2_n, C3_n = Modeling.C2 / nb2 * Modeling.e2, Modeling.C3 / nb13 * Modeling.e3  # Capacity of layer 2 and 3 for each slice
    # Matrix C : Capacities
    # capacity equals 0 for nodes on surfaces and interfaces
    C = np.zeros((nb, nb))
    for i in range(0, nb2):
        C[i, i] = C2_n
    for i in range(nb2 + 1, nb - 1):
        C[i, i] = C3_n
    # Matrix A : matrix of conductances
    A = np.zeros((nb, nb))
    A[0, 0] = -2 * G2_n
    A[0, 1] = G2_n
    for i in range(1, nb2):
        A[i, i - 1:i + 2] = np.array([G2_n, -2 * G2_n, G2_n])
    for i in range(nb2 + 1, nb13 + nb2 + 1):
        A[i, i - 1:i + 2] = np.array([G3_n, -2 * G3_n, G3_n])
    A[nb2, nb2 - 1:nb2 + 2] = np.array([G2_n, -G2_n - G3_n, G3_n])
    A[-1, -2:] = np.array([G3_n, -G3_n - hin_mean])
    # Vector B : source
    B = np.zeros(nb)
    B[-1] = hin_mean * Tin[0]
    B[0] = G2_n * T12[0]

    # ------------------------------------------------------------------------------------------------------------------
    # -----------------------------------SOLVING -----------------------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------
    Sol = np.zeros((Modeling.numstep, 3))  # matrix of solution

    # Solving
    Z = np.zeros((nb, nb))
    W = np.zeros((nb, nb))
    T = np.zeros(nb)
    V = np.zeros(nb)

    # Initial values
    T[:] = CI_RC_BC2(A, B, C, nb)

    Sol[0, :] = np.array(
        [(T[node_sup] - T[node_inf]) * (x_pos - x_inf) / (x_sup - x_inf) + T[node_inf], T[nb2], T[-1]])

    # for each time step
    for i in range(1, Modeling.numstep):
        B[0] = G2_n * T12[i]
        B[-1] = hin_mean * Tin[i]
        Z = C / Modeling.step - A
        W = C / Modeling.step
        V = np.add(np.dot(W, T), B)
        T = np.linalg.solve(Z, V)
        Sol[i, :] = np.array(
            [(T[node_sup] - T[node_inf]) * (x_pos - x_inf) / (x_sup - x_inf) + T[node_inf], T[nb2], T[-1]])

    result = pd.DataFrame(Sol, index=Modeling.xp_data.data_xp.index, columns=list_sensors)
    result = result.round(3)
    if file !='':
        result.to_csv(folder + os.sep + file + '_BC2_RC' + str(nb13) + str(nb2) + '.csv', sep=';')
    return result


def CI_RC_BC2(A, B, C, nb):
    """CI for initial temperature for
    ONLY FOR 3 LAYER WALLS (Outside / Layer 1 / Layer 2 / Layer 3 / Inside)
    Parameters:
    nb2        : Number of nodes with capacity in layer 2
    nb13       : Number of nodes with capacity in layer 1 ( = number in layer 3)
    nb         : Total number of nodes
    abSW       : Solar absoptivity of the wall, int
    abLW       : LW absorptivity of the wall, int
    Ge         : Outside heat transfert coef for conv at first step (in W/m²/K), double
    Gi         : Inside utside heat transfert coef for conv and rad at first step(in W/m²/K), double
    Te         : Outside air temperature at first step(in degC), double
    Ti         : Inside operative temperature at first step(in degC), double
    Tb         : Outside brightness temperature at first step(in degC), double
    Psol       : Vertical solar radiation on wall at first step(in W/m²), double
    C0         : Type of BC (if np.zeros (5) -> steady state conditions, else, values of Tini for the 5 nodes of interest)
    A          : Matrix of conductances
    B          : Source
    C          : Matrix of capacities
    G1_n       : Conductance of first layer for 1 slice
    pos        : location of sensor T2 = if 0, middle of the layer, if 1, between layer 2 and 3

    Return: vector of nb initial values
    """
    step = 24 * 100 * 3600.

    # Conditions initiales et resolution
    Temp0 = np.zeros(nb)
    Z = C / step - A
    W = C / step
    V = np.add(np.dot(W, Temp0), B)
    Temp0 = np.linalg.solve(Z, V)

    return Temp0
