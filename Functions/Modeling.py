import numpy as np
import pandas as pd
import sys
import Functions.Plotting as Plot
from .ModelRC import ModelRC_BC1, ModelRC_BC2


class Modeling:
    """
    Model class describes

    """

    def __init__(self, wall, num_data, xp_data):
        """

        """
        L1_i, L2_i, l3_i = 1., 1., 1.
        C1_i, C2_i, C3_i = 1e6, 1e6, 1e6
        self.e1 = wall.e1
        self.L1 = L1_i
        self.C1 = C1_i
        self.e2 = wall.e2
        self.L2 = L2_i
        self.C2 = C2_i
        self.e3 = wall.e3
        self.L3 = L2_i
        self.C3 = C2_i

        self.L = np.round(self.e1 + self.e2 + self.e3, 2)

        # if wall is symmetric, ie, layer 1 == layer 3
        self.sym = wall.symmetry
        if self.sym:
            self.L1 = self.L3
            self.C1 = self.C3

        self.num_data = num_data
        self.xp_data = xp_data

        self.start = 0  # in seconds
        self.time_start, self.time_end = xp_data.time_min, xp_data.time_max
        self.numstep = xp_data.data_xp.shape[0]
        self.step = xp_data.step  # in seconds
        self.end = self.numstep * self.step + self.start  # in seconds

        self.ab_SW = wall.abSW_out
        self.ab_LW_out = wall.abLW_out
        self.ab_LW_in = wall.abLW_in
        self.S1 = wall.S1
        self.S2 = wall.S2
        self.S3 = wall.S3
        self.pos = (wall.S3 - self.e1 - self.e2 / 2) / (self.e2 / 2)
        self.S4 = wall.S4
        self.S5 = wall.S5

        self.u_abSW = wall.u_abSW_out
        self.u_abLW_out = wall.u_abLW_out
        self.u_abLW_in = wall.u_abLW_in

        self.sigma = wall.sigma
        self.epsilon = 1e-3

    def run_model(self, type_model, BC, BC_type, hcout, list_sensors, file, folder):
        """Run model
        model   : Type of model between 'RCXXYY', 'FR' and 'QR'
        sensors    : if 1, compute 5 nodes of interest, if 0 compute sensors T2 and T23
        hcout :

        Return:
        result : matrix of temperature of 5 points (Text, T12, T2, T23 and Tin)
        """

        # Models
        if type_model[:2] == 'RC':
            type = 'RC'
            nb13 = int(type_model[2:4])
            nb2 = int(type_model[4:6])
        else:
            print("Add a new model in Modeling class!")
            exit()
        if self.sym:
            self.C3 = self.C1
            self.L3 = self.L1
        if type == 'RC':
            if BC == 1:
                result = ModelRC_BC1(self, nb13, nb2, hcout, list_sensors, file, folder)
            elif BC == 2:
                result = ModelRC_BC2(self, nb13, nb2, BC_type, list_sensors, file, folder)
            else :
                print("not possible!")
                sys.exit()
        return result

    def sens_BC(self, type_model, BC, BC_type, hcout, list_sensors, file, folder):

        df = pd.DataFrame(index=self.xp_data.data_xp.index)
        if BC ==1:
            # aSW
            ref = self.ab_SW
            sigma = self.u_abSW
            self.ab_SW = ref - sigma
            TaSWmoins = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.ab_SW = ref + sigma
            TaSWplus = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.ab_SW = ref

            # hcout
            sigma1 = 7
            self.xp_data.data_xp["hconvout_ini"] = self.xp_data.data_xp["hconv_out_ISO6946"]
            self.xp_data.data_xp["hconv_out_ISO6946"] = self.xp_data.data_xp["hconvout_ini"] - sigma1
            self.xp_data.data_xp["hconv_out_ISO6946"][self.xp_data.data_xp["hconv_out_ISO6946"] < 0] = 0
            Thcoutmoins = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.xp_data.data_xp["hconv_out_ISO6946"] = self.xp_data.data_xp["hconvout_ini"] + sigma1
            Thcoutplus = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.xp_data.data_xp["hconv_out_ISO6946"] = self.xp_data.data_xp["hconvout_ini"]

            # hLWout
            ref = self.ab_LW_out
            sigma = self.u_abLW_out
            self.ab_LW_out = ref - sigma
            ThLWoutmoins = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.ab_LW_out = ref + sigma
            ThLWoutplus = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.ab_LW_out = ref

            # hin
            sigma2 = 0.15
            self.xp_data.data_xp["h_in_ini"] = self.xp_data.data_xp["h_in"].copy()
            self.xp_data.data_xp["hr_in_ini"] = self.xp_data.data_xp["hr_in"].copy()
            self.xp_data.data_xp["hr_in"] -= sigma2
            self.xp_data.data_xp["h_in"] = self.xp_data.data_xp["hr_in"] + self.xp_data.data_xp["hconv_in"]
            Thinmoins = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.xp_data.data_xp["hr_in"] += sigma2
            self.xp_data.data_xp["h_in"] = self.xp_data.data_xp["hr_in"] + self.xp_data.data_xp["hconv_in"]
            Thinplus = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.xp_data.data_xp["h_in"] = self.xp_data.data_xp["h_in_ini"].copy()
            self.xp_data.data_xp["hr_in"] = self.xp_data.data_xp["hr_in_ini"].copy()

            sensor_names = ['Sensor S$_{out}$', 'Sensor S$_{1-2}$', 'Sensor S$_{2}$', 'Sensor S$_{2-3}$', 'Sensor S$_{in}$']

            list_names = ["ab_SW", "hconv_out_ISO6946", "hr_out", "h_in"]

            for j, name in enumerate(sensor_names):
                df[list_names[0] + name] = TaSWplus.iloc[:, j] - TaSWmoins.iloc[:, j]
                df[list_names[1] + name] = Thcoutplus.iloc[:, j] - Thcoutmoins.iloc[:, j]
                df[list_names[2] + name] = ThLWoutplus.iloc[:, j] - ThLWoutmoins.iloc[:, j]
                df[list_names[3] + name] = Thinplus.iloc[:, j] - Thinmoins.iloc[:, j]
        else :
            # T12
            sigma1 = 0.15
            self.xp_data.data_xp["T1-2_ini"] = self.xp_data.data_xp["T1-2"]
            self.xp_data.data_xp["T1-2"] =self.xp_data.data_xp["T1-2_ini"]- 0.15
            T12moins = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.xp_data.data_xp["T1-2"] =self.xp_data.data_xp["T1-2_ini"]+ 0.15
            T12plus = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.xp_data.data_xp["T1-2"] = self.xp_data.data_xp["T1-2_ini"]

            # hin
            sigma2 = 0.15
            self.xp_data.data_xp["h_in_ini"] = self.xp_data.data_xp["h_in"].copy()
            self.xp_data.data_xp["hr_in_ini"] = self.xp_data.data_xp["hr_in"].copy()
            self.xp_data.data_xp["hr_in"] -= sigma2
            self.xp_data.data_xp["h_in"] = self.xp_data.data_xp["hr_in"] + self.xp_data.data_xp["hconv_in"]
            Thinmoins = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.xp_data.data_xp["hr_in"] += sigma2
            self.xp_data.data_xp["h_in"] = self.xp_data.data_xp["hr_in"] + self.xp_data.data_xp["hconv_in"]
            Thinplus = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
            self.xp_data.data_xp["h_in"] = self.xp_data.data_xp["h_in_ini"].copy()
            self.xp_data.data_xp["hr_in"] = self.xp_data.data_xp["hr_in_ini"].copy()

            sensor_names = ['Sensor S$_{2}$', 'Sensor S$_{2-3}$',
                            'Sensor S$_{in}$']

            list_names = ["T1-2", "h_in"]

            for j, name in enumerate(sensor_names):
                df[list_names[0] + name] = T12plus.iloc[:, j] - T12moins.iloc[:, j]
                df[list_names[1] + name] = Thinplus.iloc[:, j] - Thinmoins.iloc[:, j]
        df.to_csv('Figures/2-df_sens_BC'+str(BC)+'.csv')
        # PLOT
        Plot.sens_BC_plot(self, df, sensor_names, list_names, BC)

        return df  # , Cov, Cor

    def sens_param(self, type_model, BC, BC_type, hcout, list_sensors, file, folder):

        df_sens = pd.DataFrame(index=self.xp_data.data_xp.index)

        # L13
        var = 0.1  # Standard deviation of noise (Remy et Degiovanni 2005)
        ref = self.L1
        self.L1 = (1 - var) * ref
        TL13moins = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
        self.L1 = (1 + var) * ref
        TL13plus = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
        self.L1 = ref

        # L2
        ref = self.L2
        self.L2 = (1 - var) * ref
        TL2moins = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
        self.L2 = (1 + var) * ref
        TL2plus = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
        self.L2 = ref

        # C13
        ref = self.C1
        self.C1 = (1 - var) * ref
        TC13moins = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
        self.C1 = (1 + var) * ref
        TC13plus = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
        self.C1 = ref

        # C2
        ref = self.C2
        self.C2 = (1 - var) * ref
        TC2moins = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
        self.C2 = (1 + var) * ref
        TC2plus = self.run_model(type_model, BC, BC_type, hcout, list_sensors, file, folder)
        self.C2 = ref

        if BC ==1 :
            sensor_names = ['Sensor S$_{out}$', 'Sensor S$_{1-2}$', 'Sensor S$_{2}$', 'Sensor S$_{2-3}$', 'Sensor S$_{in}$']
        else :
            sensor_names = [ 'Sensor S$_{2}$', 'Sensor S$_{2-3}$', 'Sensor S$_{in}$']

        list_names = ["L13", "L2", "C13", "C2"]

        for j, name in enumerate(sensor_names):
            df_sens[list_names[0] + name] = (TL13plus.iloc[:, j] - TL13moins.iloc[:, j]) / (2 * var)
            df_sens[list_names[1] + name] = (TL2plus.iloc[:, j] - TL2moins.iloc[:, j]) / (2 * var)
            df_sens[list_names[2] + name] = (TC13plus.iloc[:, j] - TC13moins.iloc[:, j]) / (2 * var)
            df_sens[list_names[3] + name] = (TC2plus.iloc[:, j] - TC2moins.iloc[:, j]) / (2 * var)
        # df.to_csv('df_sens_param.csv')
        # TL13plus.to_csv('TL13plus.csv')
        # TL13moins.to_csv('TL13moins.csv')
        # # ------------------------COVARIANCE AND CORRELATION-------------------------------------------#
        Cov = np.zeros((4, 4, 4))
        # X_sq = np.zeros((4, 4))
        # SigmaN = 0.1
        # X_sq = np.dot(np.transpose(X[:, :4, 2]), X[:, :4, 2])
        # Cov[:, :, 2] = SigmaN ** 2 * np.linalg.inv(X_sq)
        # Cov3 = np.zeros((4, 4))
        # X_sq = np.dot(np.transpose(X[:, 0:4, 3]), X[:, 0:4, 3])
        # Cov3 = SigmaN ** 2 * np.linalg.inv(X_sq)
        Cor = np.zeros((4, 4, 4))
        # for k in range(2, 3):
        #     for i in range(0, 4):
        #         for j in range(0, 4):
        #             Cor[i, j, k] = Cov[i, j, k] / np.sqrt(Cov[i, i, k] * Cov[j, j, k])
        # Cor3 = np.zeros((4, 4))
        # for i in range(0, 4):
        #     for j in range(0, 4):
        #         Cor3[i, j] = Cov3[i, j] / np.sqrt(Cov3[i, i] * Cov3[j, j])
        # Cor, Cor3 = np.around(Cor, decimals=2), np.around(Cor3, decimals=2)
        # Cov, Cov3 = np.around(Cov, decimals=4), np.around(Cov3, decimals=4)
        # print('COR2', Cor[:, :, 2])
        # print('COV2', Cov[:, :, 2])
        # print('COR3', Cor3)
        # print('COV3', Cov3)
        # print('---------------------')
        sensor_names = ['Sensor S$_{2}$', 'Sensor S$_{2-3}$', 'Sensor S$_{in}$']
        Plot.sens_param_plot(self, df_sens, sensor_names, list_names, BC)
        df_sens.to_csv('Figures/2-df_sens_param-BC'+str(BC)+'.csv')
        return df_sens, Cov, Cor


    def compare_model(self, data_true, data_test, list_sensors, ext, model):

        df = pd.DataFrame(index=data_true.index)
        for j, name in enumerate(list_sensors):
            df[name] = np.abs(data_test.loc[:, name] - data_true.loc[:, name + ext])

        # Dataframe to store residuals
        if len(list_sensors) == 5:
#            res_df = pd.DataFrame([[model, df[list_sensors[0]].mean().round(2), df[list_sensors[1]].mean().round(2),
#                                    df[list_sensors[2]].mean().round(2), df[list_sensors[3]].mean().round(2),
#                                    df[list_sensors[4]].mean().round(2)]],
#                                  columns=['model', 'res Tsout', 'res T1-2', 'res T2', 'res T2-3', 'res Tsin'])
            res_df = pd.DataFrame([[model, round(df[list_sensors[0]].mean(),2), round(df[list_sensors[1]].mean(),2),
                                    round(df[list_sensors[2]].mean(),2), round(df[list_sensors[3]].mean(),2),
                                    round(df[list_sensors[4]].mean(),2)]],
                                  columns=['model', 'res Tsout', 'res T1-2', 'res T2', 'res T2-3', 'res Tsin'])

        elif len(list_sensors) == 3:
#            res_df = pd.DataFrame([[model, df[list_sensors[0]].mean().round(2), df[list_sensors[1]].mean().round(2),
#                                    df[list_sensors[2]].mean().round(2)]],
#                                    columns=['model', 'res T2', 'res T2-3', 'res Tsin'])
            res_df = pd.DataFrame([[model, round(df[list_sensors[0]].mean(),2), round(df[list_sensors[1]].mean(),2),
                                    round(df[list_sensors[2]].mean(),2)]],
                                    columns=['model', 'res T2', 'res T2-3', 'res Tsin'])
        else:
            print("not possible!")
            sys.exit()

        return res_df


def init(wall, xp_data, num_data):
    """
    """
    modeling = Modeling(wall, xp_data, num_data)
    return modeling
