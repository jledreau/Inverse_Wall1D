import os
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import windrose # Install it with : pip install git+https://github.com/python-windrose/windrose
from datetime import datetime, timedelta, date
import pandas as pd

def cumulative_freq_diagram(xp_data, h_list, folder):
    # legend
    data_graph = xp_data.data_xp[h_list]
    lines = []
    for i in range(len(h_list)):
        lines += [xp_data.xp_lgd[[key for key in xp_data.xp_lgd.keys() if key == str(h_list[i])][0]]]
    l_l, l_c, l_s, l_m = [l[0] for l in lines], [l[1] for l in lines], [l[2] for l in lines], [l[3] for l in lines]

    x = np.zeros((len(h_list), len(data_graph)))
    for i in range(len(h_list)):
        x[i] = np.sort(data_graph[h_list[i]])
    y = np.arange(1, len(x[0]) + 1) / len(x[0])

    fig, ax = plt.subplots(figsize=(6, 4))

    lim_max = np.ceil((np.max(x[:, :]) / 5) * 5)
    for i in range(len(h_list)):
        _ = ax.hlines(1, np.max(x[i]), lim_max, colors=l_c[i])
        _ = ax.plot(x[i, :], y[:], marker=l_m[i], linestyle=l_s[i], linewidth=2, color=l_c[i], label=l_l[i])
    ax.grid(True)
    ax.legend(loc='right')
    ax.set_xlabel('heat transfer coefficient [W/m²/K]')
    ax.set_ylabel('Cumulative frequency')
    plt.xlim(0, lim_max)
    plt.ylim(0, 1.01)
    plt.savefig('Figures' + os.sep + '1_cumulative_periodigram_h.svg', dpi=150)

    # Statistics
    data_graph.describe().to_csv(folder + os.sep + '1_cumulative_periodigram_h_stat.csv', sep=',')


def onegraph_plot(data, list, ylabel, title, folder):
    """
    FCT :
    INPUTS :
            * name_folder :
    :return:
    """
    data_graph = data.data_xp[list]
    lines = []
    for i in range(len(list)):
        lines += [data.xp_lgd[[key for key in data.xp_lgd.keys() if key == str(list[i])][0]]]
    l_l, l_c, l_s, l_m = [l[0] for l in lines], [l[1] for l in lines], [l[2] for l in lines], [l[3] for l in lines]

    time_1, time_2 = data_graph.index.min(), data_graph.index.max()
    myFmt = mdates.DateFormatter('%d/%m')  # http://strftime.org/
    days = mdates.DayLocator()
    fig1 = plt.figure(figsize=(10, 8))
    ax = fig1.add_subplot(111)
    i = 0
    for col in list:
        ax.plot(data_graph.index.values, data_graph[col], color=l_c[i], alpha=0.7, label=l_l[i], linewidth=1,
                linestyle=l_s[i])
        i += 1
    plt.ylabel(ylabel, fontsize=12)
    plt.xlim([time_1, time_2])
    ax.legend(loc=2, prop={'size': 12})
    plt.grid(True)
    plt.tight_layout()
    ax.set_xlabel('')
    ax.get_xaxis().set_major_locator(days)
    ax.get_xaxis().set_major_formatter(myFmt)
    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    plt.savefig(folder + '\\' + title + '.svg', dpi=150)


def twograph_BC_InverseMethod_plot(data, list_left, ylabel_left, list_right, ylabel_right, delta_init, delta_valid,
                                   title, folder):
    """
    FCT :
    INPUTS :
            * name_folder :
    :return:
    """
    data_graph = data.data_xp[list_left + list_right]
    lines_left, lines_right = [], []
    for i in range(len(list_left)):
        lines_left += [data.xp_lgd[[key for key in data.xp_lgd.keys() if key == str(list_left[i])][0]]]
    l_ll, l_lc, l_ls, l_lm = [l[0] for l in lines_left], [l[1] for l in lines_left], [l[2] for l in lines_left], [l[3]
                                                                                                                  for l
                                                                                                                  in
                                                                                                                  lines_left]
    for i in range(len(list_right)):
        lines_right += [data.xp_lgd[[key for key in data.xp_lgd.keys() if key == str(list_right[i])][0]]]
    l_rl, l_rc, l_rs, l_rm = [l[0] for l in lines_right], [l[1] for l in lines_right], [l[2] for l in lines_left], [l[3]
                                                                                                                    for
                                                                                                                    l
                                                                                                                    in
                                                                                                                    lines_right]
    time_1, time_2 = data_graph.index.min(), data_graph.index.max()

    myFmt = mdates.DateFormatter('%d/%m')  # http://strftime.org/
    days = mdates.DayLocator()
    fig1 = plt.figure(figsize=(10, 8))
    ax21 = fig1.add_subplot(111)

    i = 0
    for col in list_left:
        ax21.plot(data_graph.index.values, data_graph[col], color=l_lc[i], alpha=0.7, label=l_ll[i], linewidth=1,
                  linestyle=l_ls[i])
        i += 1

    plt.ylabel(ylabel_left, fontsize=12)
    lim_left_min, lim_left_max = np.floor((np.min(data_graph[list_left].min()) / 5) * 5) - 5, np.ceil(
        (np.max(data_graph[list_left].max()) / 5) * 5)
    ax21.set_ylim([lim_left_min, lim_left_max])
    plt.xlim([time_1, time_2])
    ax21.legend(loc=2, prop={'size': 14})
    plt.yticks(fontsize=14)
    plt.setp(ax21.get_xticklabels(), visible=False)

    ax22 = fig1.add_subplot(111, sharex=ax21, frameon=False)
    i = 0
    for col in list_right:
        ax22.plot(data_graph.index.values, data_graph[col], color=l_rc[i], alpha=0.7, label=l_rl[i], linewidth=1,
                  linestyle=l_rs[i])
        i += 1
    ax22.yaxis.label.set_color(l_rc[0])
    ax22.tick_params(axis='y', colors=l_rc[0])

    lim_right_min, lim_right_max = np.floor((np.min(data_graph[list_right].min()) / 100) * 100) - 100, np.ceil(
        (np.max(data_graph[list_right].max()) / 100) * 100 + 100)
    ax22.set_ylim([lim_right_min, lim_right_max])

    time_ini = time_1 + delta_init
    plt.axvspan(time_1, time_ini, facecolor='silver', alpha=0.4)
    plt.axvline(x=time_ini, color='k', linewidth=2)
    time_end = time_1 + delta_valid
    plt.axvspan(time_end, time_2, facecolor='silver', alpha=0.4)
    plt.axvline(x=time_end, color='k', linewidth=2)
    ax22.text(time_ini - delta_init / 2, lim_right_min + 5, 'Init', horizontalalignment='left',
              verticalalignment='bottom', rotation=0, fontsize=14)
    ax22.text(time_1 + (time_2 - time_1) / 2, lim_right_min + 5, 'Identification', horizontalalignment='left',
              verticalalignment='bottom', rotation=0, fontsize=14)
    ax22.text(time_1 + delta_valid, lim_right_min + 5, 'Validation', horizontalalignment='left',
              verticalalignment='bottom', rotation=0, fontsize=14)

    ax22.yaxis.tick_right()
    ax22.yaxis.set_label_position("right")
    plt.ylabel(ylabel_right, fontsize=12)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xticks(rotation=12)
    ax22.set_xlim([time_1, time_2])
    ax22.legend(loc=1, prop={'size': 14})
    ax22.xaxis.set_minor_locator(days)
    ax22.xaxis.set_major_formatter(myFmt)
    ax21.xaxis.set_minor_locator(days)
    ax21.xaxis.set_major_formatter(myFmt)

    plt.grid(True)
    plt.tight_layout()
    plt.savefig(folder + '\\' + title + '.svg', dpi=150)


def windrose_plot(data, wind_speed, wind_dir, title, folder):
    # windrose from https://github.com/python-windrose/windrose
    # Install it with : pip install git+https://github.com/python-windrose/windrose
    ws = data.data_xp[wind_speed].to_numpy()
    wd = data.data_xp[wind_dir].to_numpy()
    ax = windrose.WindroseAxes.from_ax()
    wind_lim_max = np.ceil((np.max(ws.max()) / 2) * 2)
    bins_range = np.arange(0, wind_lim_max, 2)  # this sets the legend scale
    ax.bar(wd, ws, normed=True, opening=0.8, edgecolor='white', bins=bins_range)
    ax.set_yticks(np.arange(2, 15, step=2))
    ax.set_yticklabels(np.arange(2, 15, step=2), fontsize=12)
    ax.set_legend()
    ax.legend(loc=(0.92, 0.7), title='Wind speed [m/s]', fontsize=12)
    ax.get_legend().get_title().set_fontsize('12')

    plt.grid(True)
    # plt.tight_layout()
    plt.savefig(folder + '\\' + title + '.svg', dpi=150)


def windtime_plot(data, wind_speed, wind_dir, title, folder):
    # wind against time
    time_1, time_2 = data.data_xp.index.min(), data.data_xp.index.max()
    myFmt = mdates.DateFormatter('%d/%m')  # http://strftime.org/
    days = mdates.DayLocator()  # every month
    fig1 = plt.figure(figsize=(9, 6))
    ax0 = fig1.add_subplot(111)
    plt.setp(ax0.get_xticklabels(), visible=False)

    lines = []
    lines += [data.xp_lgd[[key for key in data.xp_lgd.keys() if key == str(wind_speed)][0]]]
    lines += [data.xp_lgd[[key for key in data.xp_lgd.keys() if key == str(wind_dir)][0]]]
    l_l, l_c, l_s, l_m = [l[0] for l in lines], [l[1] for l in lines], [l[2] for l in lines], [l[3] for l in lines]

    ax0.plot(data.data_xp.index, data.data_xp[wind_speed], label=l_l[0], color=l_c[0], alpha=0.7, linewidth=1.5)
    plt.ylabel('wind speed $[m.s^{-1}]$', fontsize=14)
    ax0.set_yticks(np.arange(0, 22, step=4.5))
    ax0.set_ylim([0, 18])
    ax0.legend(loc=2, prop={'size': 14})
    plt.yticks(fontsize=14)
    ax0.yaxis.label.set_color(l_c[0])
    ax0.tick_params(axis='y', colors=l_c[0])

    ax00 = fig1.add_subplot(111, sharex=ax0, frameon=False)
    ax00.scatter(data.data_xp.index, data.data_xp[wind_dir], label=l_l[1], marker=l_m[1], color=l_c[1], alpha=0.7)
    ax00.yaxis.label.set_color(l_c[1])
    ax00.set_yticks(np.arange(0, 450, step=90))
    ax00.tick_params(axis='y', colors=l_c[1])
    ax00.yaxis.tick_right()
    ax00.yaxis.set_label_position("right")
    plt.ylabel('wind direction $[°]$', fontsize=14)
    ax00.set_ylim([0, 360])
    plt.yticks(fontsize=14)
    ax00.legend(loc=1, prop={'size': 14})
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xticks(rotation=12)
    ax00.set_xlim([time_1, time_2])
    ax00.xaxis.set_minor_locator(days)
    ax00.xaxis.set_major_formatter(myFmt)

    plt.grid(True)
    # plt.tight_layout()
    plt.savefig(folder + '\\' + title + '.svg', dpi=150)


def sens_BC_plot(Modeling, df, sensor_names, list_names, BC):
    if BC ==1:
        nb_ax = 5
    else :
        nb_ax=3
    fig, axes = plt.subplots(nb_ax, 1, figsize=(8, 10), sharex=True)

    lines = []
    for name in list_names:
        lines += [Modeling.xp_data.xp_lgd[[key for key in Modeling.xp_data.xp_lgd.keys() if key in str(name)][0]]]
    l_ll, l_lc, l_ls, l_lm = [l[0] for l in lines], [l[1] for l in lines], [l[2] for l in lines], [l[3] for l in lines]

    # λ, ρ, ε
    l_ll = [w.replace('lambda', 'λ') for w in l_ll]
    l_ll = [w.replace('rho', 'ρ') for w in l_ll]
    l_ll = [w.replace('epsilon', 'ε') for w in l_ll]


    range_y, lim_l, lim_h = 4, -12, 8
    Styl = ['-', '--']

    for i in range(nb_ax):
        axes[i].set_title(sensor_names[i], fontsize=14)
        for j in range(len(list_names)):
            axes[i].plot(df.index.values, df[list_names[j]+sensor_names[i]], color=l_lc[j], alpha=0.7, label=l_ll[j],
                      linewidth=1,
                      linestyle=l_ls[j])

        axes[i].yaxis.set_ticks(np.arange(lim_l, lim_h + range_y, range_y))
        axes[i].set_ylim([lim_l-2, lim_h])
        axes[i].set_xlim([df.index.min(), df.index.max()])

        if BC ==1 :
            if i != 4:
                plt.setp(axes[i].get_xticklabels(), visible=False)
            if i == 2:
                axes[i].set_ylabel('Difference of temperature [°C]', fontsize=14)
            if i == 4:
                # Legend below figure
                if (Modeling.numstep*Modeling.step)/3600/24 < 2:
                    loc = mdates.HourLocator(interval=3)
                    TS_fmt = mdates.DateFormatter('%Hh')
                elif (Modeling.numstep*Modeling.step)/3600/24 < 7:
                    loc = mdates.DayLocator(interval=1)
                    TS_fmt = mdates.DateFormatter('%d/%m/%y')
                elif (Modeling.numstep*Modeling.step)/3600/24 >15:
                    loc = mdates.DayLocator(interval=2)
                    TS_fmt = mdates.DateFormatter('%d/%m/%y')
                else:
                    loc = mdates.MonthLocator()
                    TS_fmt = mdates.DateFormatter('%d/%m/%y')

                axes[i].xaxis.set_minor_locator(loc)
                axes[i].xaxis.set_major_formatter(TS_fmt)
                box = axes[i].get_position()
                axes[i].set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
                axes[i].legend(loc='upper center', bbox_to_anchor=(0.55, -0.2), fancybox=True, shadow=False, ncol=4,
                            facecolor='white', \
                            fontsize=14, framealpha=1)
        else :
            if i != 2:
                plt.setp(axes[i].get_xticklabels(), visible=False)
            if i == 1:
                axes[i].set_ylabel('Difference of temperature [°C]', fontsize=14)
            if i == 2:
                # Legend below figure
                if (Modeling.numstep * Modeling.step) / 3600 / 24 < 2:
                    loc = mdates.HourLocator(interval=3)
                    TS_fmt = mdates.DateFormatter('%Hh')
                elif (Modeling.numstep * Modeling.step) / 3600 / 24 < 7:
                    loc = mdates.DayLocator(interval=1)
                    TS_fmt = mdates.DateFormatter('%d/%m/%y')
                elif (Modeling.numstep * Modeling.step) / 3600 / 24 > 15:
                    loc = mdates.DayLocator(interval=2)
                    TS_fmt = mdates.DateFormatter('%d/%m/%y')
                else:
                    loc = mdates.MonthLocator()
                    TS_fmt = mdates.DateFormatter('%d/%m/%y')

                axes[i].xaxis.set_minor_locator(loc)
                axes[i].xaxis.set_major_formatter(TS_fmt)
                box = axes[i].get_position()
                axes[i].set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
                axes[i].legend(loc='upper center', bbox_to_anchor=(0.55, -0.2), fancybox=True, shadow=False, ncol=4,
                               facecolor='white', \
                               fontsize=14, framealpha=1)

    plt.subplots_adjust(hspace=0.4)
    # plt.tight_layout()
    plt.savefig('Figures/2-Sens_BC'+str(BC)+'.svg')


def sens_param_plot(Modeling, df, sensor_names, list_names, BC):
    nb_ax = 3
    fig, axes = plt.subplots(nb_ax, 1, figsize=(7, 7), sharex=True)
    range_y, lim_l, lim_h = 2, -4, 4
    lines = []

    for name in list_names:
        lines += [Modeling.xp_data.xp_lgd[[key for key in Modeling.xp_data.xp_lgd.keys() if key in str(name)][0]]]
    l_ll, l_lc, l_ls, l_lm = [l[0] for l in lines], [l[1] for l in lines], [l[2] for l in lines], [l[3] for l in lines]
    # λ, ρ, ε
    l_ll = [w.replace('lambda', 'λ') for w in l_ll]
    l_ll = [w.replace('rho', 'ρ') for w in l_ll]
    l_ll = [w.replace('epsilon', 'ε') for w in l_ll]

    Styl = ['-', 'dashed']

    for i in range(nb_ax):
        axes[i].set_title(sensor_names[i], fontsize=14)
        for j in range(len(list_names)):
            axes[i].plot(df.index.values, df[list_names[j]+sensor_names[i]], color=l_lc[j], alpha=0.7, label=l_ll[j],
                      linewidth=1,
                      linestyle=l_ls[j])

        axes[i].yaxis.set_ticks(np.arange(lim_l, lim_h + range_y, range_y))
        axes[i].set_ylim([lim_l, lim_h])
        axes[i].set_xlim([df.index.min(), df.index.max()])

        axes[i].grid(True)
        axes[i].axhline(y=0, color='black')
        axes[i].yaxis.set_ticks(np.arange(lim_l, lim_h + range_y, range_y))
        axes[i].set_xlim([df.index.min(), df.index.max()])
        if i != 2:
            plt.setp(axes[i].get_xticklabels(), visible=False)
        if i == 1:
            axes[i].set_ylabel('Reduced sensitivity [°C]', fontsize=12)
        if i == 2:
            # Legend below figure
            if (Modeling.numstep * Modeling.step) / 3600 / 24 < 2:
                loc = mdates.HourLocator(interval=3)
                TS_fmt = mdates.DateFormatter('%Hh')
            elif (Modeling.numstep * Modeling.step) / 3600 / 24 < 7:
                loc = mdates.DayLocator(interval=1)
                TS_fmt = mdates.DateFormatter('%d/%m/%y')
            elif (Modeling.numstep * Modeling.step) / 3600 / 24 > 15:
                loc = mdates.DayLocator(interval=2)
                TS_fmt = mdates.DateFormatter('%d/%m/%y')
            else:
                loc = mdates.MonthLocator()
                TS_fmt = mdates.DateFormatter('%d/%m/%y')

            axes[i].xaxis.set_minor_locator(loc)
            axes[i].xaxis.set_major_formatter(TS_fmt)
            box = axes[i].get_position()
            axes[i].set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
            axes[i].legend(loc='upper center', bbox_to_anchor=(0.55, -0.2), fancybox=True, shadow=False, ncol=4,
                           facecolor='white', \
                           fontsize=14, framealpha=1)
            # axes[i].annotate('Journée ensoleillée', xy=(0.07, 0.055), xycoords='figure fraction', fontsize=12,
            #               color='k')
            # axes[i].annotate('Journée nuageuse', xy=(0.07, 0.02), xycoords='figure fraction', fontsize=12, color='k')
        plt.subplots_adjust(hspace=0.3)
    plt.savefig('Figures/2-Sens_param-BC'+str(BC)+'.svg')


def sens_BC_plot_2days(Modeling, df, sensor_names, list_names,start_day1, start_day2, name_day1, name_day2, file, BC):
    if BC == 1:
        nb_ax = 5
    else:
        nb_ax = 3
    fig, axes = plt.subplots(nb_ax, 1, figsize=(8, 10), sharex=True)

    lines = []
    for name in list_names:
        lines += [
            Modeling.xp_data.xp_lgd[[key for key in Modeling.xp_data.xp_lgd.keys() if key in str(name)][0]]]
    l_ll, l_lc, l_ls, l_lm = [l[0] for l in lines], [l[1] for l in lines], [l[2] for l in lines], [l[3] for
                                                                                                   l in
                                                                                                   lines]
    # λ, ρ, ε
    l_ll = [w.replace('lambda', 'λ') for w in l_ll]
    l_ll = [w.replace('rho', 'ρ') for w in l_ll]
    l_ll = [w.replace('epsilon', 'ε') for w in l_ll]

    range_y, lim_l, lim_h = 4, -12, 8
    Styl = ['-', '-.']

    data_day1 = df[df.index.date == start_day1]
    data_day2 = df[df.index.date == start_day2]

    for i in range(nb_ax):
        axes[i].set_title(sensor_names[i], fontsize=14)
        for j in range(len(list_names)):
            axes[i].plot(data_day1.index.values, data_day1[list_names[j] + sensor_names[i]], color=l_lc[j],
                         alpha=0.7,
                         label=l_ll[j],
                         linewidth=2,
                         linestyle=Styl[0])
            axes[i].plot(data_day1.index.values, data_day2[list_names[j] + sensor_names[i]], color=l_lc[j],
                         alpha=0.7,
                         label=l_ll[j],
                         linewidth=2,
                         linestyle=Styl[1])

        axes[i].yaxis.set_ticks(np.arange(lim_l, lim_h + range_y, range_y))
        axes[i].set_ylim([lim_l - 2, lim_h])
        axes[i].set_xlim([data_day1.index.min(), data_day1.index.max()])
        if BC ==1:
            if i != 4:
                plt.setp(axes[i].get_xticklabels(), visible=False)
            if i == 2:
                axes[i].set_ylabel('Difference of temperature [°C]', fontsize=14)
            if i == 4:
                # Legend below figure
                loc = mdates.HourLocator(interval=3)
                TS_fmt = mdates.DateFormatter('%Hh')

                axes[i].xaxis.set_minor_locator(loc)
                axes[i].xaxis.set_major_formatter(TS_fmt)
                box = axes[i].get_position()
                axes[i].set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
                axes[i].legend(loc='upper center', bbox_to_anchor=(0.55, -0.2), fancybox=True, shadow=False,
                               ncol=4,
                               facecolor='white', \
                               fontsize=10, framealpha=1)
                axes[i].annotate(name_day1, xy=(0.15, 0.065), xycoords='figure fraction', fontsize=10, color='k')
                axes[i].annotate(name_day2, xy=(0.15, 0.038), xycoords='figure fraction', fontsize=10, color='k')
        else :
            if i != 2:
                plt.setp(axes[i].get_xticklabels(), visible=False)
            if i == 1:
                axes[i].set_ylabel('Difference of temperature [°C]', fontsize=14)
            if i == 2:
                # Legend below figure
                loc = mdates.HourLocator(interval=3)
                TS_fmt = mdates.DateFormatter('%Hh')

                axes[i].xaxis.set_minor_locator(loc)
                axes[i].xaxis.set_major_formatter(TS_fmt)
                box = axes[i].get_position()
                axes[i].set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
                axes[i].legend(loc='upper center', bbox_to_anchor=(0.55, -0.2), fancybox=True, shadow=False,
                               ncol=4,
                               facecolor='white', \
                               fontsize=10, framealpha=1)
                axes[i].annotate(name_day1, xy=(0.15, 0.065), xycoords='figure fraction', fontsize=10, color='k')
                axes[i].annotate(name_day2, xy=(0.15, 0.038), xycoords='figure fraction', fontsize=10, color='k')
    plt.subplots_adjust(hspace=0.4)
    # plt.tight_layout()
    plt.savefig('Figures/'+file+'.svg')

def sens_param_plot_2days(Modeling, df, sensor_names, list_names, start_day1, start_day2, name_day1, name_day2, file, BC):

    nb_ax = 3
    fig, axes = plt.subplots(nb_ax, 1, figsize=(7, 7), sharex=True)
    range_y, lim_l, lim_h = 2, -4, 4
    lines = []

    for name in list_names:
        lines += [
            Modeling.xp_data.xp_lgd[[key for key in Modeling.xp_data.xp_lgd.keys() if key in str(name)][0]]]
    l_ll, l_lc, l_ls, l_lm = [l[0] for l in lines], [l[1] for l in lines], [l[2] for l in lines], [l[3] for
                                                                                                   l in
                                                                                                   lines]
    # λ, ρ, ε
    l_ll = [w.replace('lambda', 'λ') for w in l_ll]
    l_ll = [w.replace('rho', 'ρ') for w in l_ll]
    l_ll = [w.replace('epsilon', 'ε') for w in l_ll]

    Styl = ['-', '-.']
    data_day1 = df[df.index.date == start_day1]
    data_day2 = df[df.index.date == start_day2]
    for i in range(nb_ax):
        axes[i].set_title(sensor_names[i], fontsize=14)
        for j in range(len(list_names)):
            axes[i].plot(data_day1.index.values, data_day1[list_names[j] + sensor_names[i]], color=l_lc[j],
                         alpha=0.7,
                         label=l_ll[j],
                         linewidth=2,
                         linestyle=Styl[0])
            axes[i].plot(data_day1.index.values, data_day2[list_names[j] + sensor_names[i]], color=l_lc[j],
                         alpha=0.7,
                         label=l_ll[j],
                         linewidth=2,
                         linestyle=Styl[1])

        axes[i].yaxis.set_ticks(np.arange(lim_l, lim_h + range_y, range_y))
        axes[i].set_ylim([lim_l, lim_h])
        axes[i].set_xlim([data_day1.index.min(), data_day1.index.max()])

        axes[i].grid(True)
        axes[i].axhline(y=0, color='black')
        axes[i].yaxis.set_ticks(np.arange(lim_l, lim_h + range_y, range_y))
        if i != 2:
            plt.setp(axes[i].get_xticklabels(), visible=False)
        if i == 1:
            axes[i].set_ylabel('Reduced sensitivity [°C]', fontsize=12)
        if i == 2:
            # Legend below figure
            loc = mdates.HourLocator(interval=3)
            TS_fmt = mdates.DateFormatter('%Hh')

            axes[i].xaxis.set_minor_locator(loc)
            axes[i].xaxis.set_major_formatter(TS_fmt)
            box = axes[i].get_position()
            axes[i].set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
            axes[i].legend(loc='upper center', bbox_to_anchor=(0.55, -0.2), fancybox=True, shadow=False,
                           ncol=4,
                           facecolor='white', \
                           fontsize=10, framealpha=1)
            axes[i].annotate(name_day1, xy=(0.12, 0.045), xycoords='figure fraction', fontsize=10,
                          color='k')
            axes[i].annotate(name_day2, xy=(0.12, 0.01), xycoords='figure fraction', fontsize=10, color='k')
        plt.subplots_adjust(hspace=0.3)
    plt.savefig('Figures/'+file+'.svg')