**Welcome to Inverse Wall 1D !**

The objective of this study is to evaluate the ability of inverse techniques to estimate the resistance and the capacity of a highly insulated multilayer wall under real weather conditions. The wall is equipped with temperature sensors inside and on its inner and outer surfaces and the boundary conditions have been measured over a 14-day period. From a sensitivity analysis, we demonstrate a high correlation with some parameters defining the thermal performance of the walls (thermal resistance or capacity). A solution is proposed to limit the number of identified parameters, while allowing the identification of the thermal resistance and the thermal capacity of the walls. Two cases arise: either the weather conditions are accurately measured (temperature, short- and long-wave radiation) and the assessment of the thermal characteristics is possible or intrusive sensors are installed and the assessment of the thermal characteristics is more precise. 


**Preview**

Figure 1:

![](image/Figure1.png)

Figure 2:

![](image/Figure2.png)



**Release history**
*  v0 : 17/05/2023


**License**

Licensed by La Rochelle University/LaSIE under a BSD 3 license (https://opensource.org/licenses/BSD-3-Clause).


**References**

Manon Rendua, Jérôme Le Dréau, Patrick Salagnaca, Maxime Doya, Assessing the influence of boundary conditions when estimating thermal characteristics of insulated building walls under real weather conditions. Under review (2023).



