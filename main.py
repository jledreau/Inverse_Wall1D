import datetime
import os
import sys
import time
from tkinter.filedialog import askopenfilename
from datetime import date

import pandas as pd

from Components import Wall, XP_data, NUM_data
from Functions import Plotting, Modeling

pd.set_option('display.width', 320)
pd.set_option('display.max_columns', 12)

if __name__ == '__main__':
    # --------------------------- STEP 1 / Implementing the case study parameters and data ----------------------------#
    # Wall type
    json_name = ""
    while json_name == "":
        json_name = askopenfilename(filetypes=(('json files', '*.json'),), initialdir=os.getcwd() + '\Case_study\Walls',
                                    title="Choose the file describing your wall (.json format) : ")
    wall_case = Wall.init(json_name)
    time.sleep(1)

    # XP data
    json_name = ""
    while json_name == "":
        json_name = askopenfilename(filetypes=(('json files', '*.json'),),
                                    initialdir=os.getcwd() + '\Case_study\Measured_experimental_data',
                                    title="Choose the experimental data (.json format) : ")
    xp_data = XP_data.init(json_name, wall_case)
    time.sleep(1)

    # NUM data
    json_name = ""
    while json_name == "":
        json_name = askopenfilename(filetypes=(('json files', '*.json'),),
                                    initialdir=os.getcwd() + '\Case_study\Synthetic_experimental_data',
                                    title="Choose the experimental data (.json format) : ")
    num_data = NUM_data.init(json_name, wall=wall_case, index=xp_data.data_xp.index)
    time.sleep(1)

    # ----------------------------------- STEP 2 / Plot data (BC, h and temperatures) ---------------------------------#
    ## ------ Plot h, cumulative frequency diagram
    Plotting.cumulative_freq_diagram(xp_data, h_list=["h_in", "hconv_out_ISO6946", "hr_out"], folder="Figures")

    ## ------ Plot BC and h
    Plotting.onegraph_plot(xp_data, ['Tair_out', 'T_in', 'T_b'], "Temperature [°C]", "1-BC_temperature", "Figures")
    Plotting.onegraph_plot(xp_data, ['P_SW', 'P_LW'], 'Power density [W/m²]', "1-BC_out radiation", "Figures")
    Plotting.onegraph_plot(xp_data, ['Tsout', 'T1-2', 'T2', 'T2-3', 'Tsin'], "Temperature [°C]", "1-Inside temperature",
                           "Figures")
    Plotting.onegraph_plot(xp_data, ['hconv_in', 'hr_in', 'hr_out'], "h [W/m²/K]", "1-h other", "Figures")
    Plotting.onegraph_plot(xp_data, ['hconv_out_ISO6946', 'hconvout_const', 'hconvout_Liu', 'hconvout_ahsrae'],
                           "h [W/m²/K]", "1-h conv out", "Figures")

    Plotting.twograph_BC_InverseMethod_plot(xp_data, ['Tair_out', 'Tair_in', 'TLW_in', 'T_b'], "Temperature [°C]",
                                            ['P_SW'],
                                            "Vertical solar radiation [W/m²]", datetime.timedelta(days=1, hours=6),
                                            datetime.timedelta(days=12, hours=6), "1-BC_experimental conditions in out",
                                            "Figures")

    Plotting.windrose_plot(xp_data, "wind_speed_2.5m", "wind_dir", "1-windrose", "Figures")
    Plotting.windtime_plot(xp_data, "wind_speed_2.5m", "wind_dir", "1-wind_time", "Figures")

    ## ------ Model configuration
    model_type_list = ['RC0511']
    modeling_config = Modeling.Modeling(wall_case, num_data, xp_data)
    for model_ in model_type_list:
        modeling_config.L1 = wall_case.L1
        modeling_config.L2 = wall_case.L2
        modeling_config.C1 = wall_case.C1
        modeling_config.C2 = wall_case.C2

        result_BC1 = modeling_config.run_model(model_, BC=1, BC_type="num", hcout='hconv_out_ISO6946',
                                               list_sensors=['Tsout', 'T1-2', 'T2', 'T2-3', 'Tsin'],
                                               file="1-Model for comparison with numerical data",
                                               folder="Case_study/Modeling")

        result_BC2 = modeling_config.run_model(model_, BC=2, BC_type="num", hcout='',
                                               list_sensors=['T2', 'T2-3', 'Tsin'],
                                               file="1-Model for comparison with numerical data",
                                               folder="Case_study/Modeling")

        # calculate residuals compared to numerical data
        res = modeling_config.compare_model(data_true=modeling_config.num_data.data_num_raw, data_test=result_BC1,
                                            list_sensors=['Tsout', 'T1-2', 'T2', 'T2-3', 'Tsin'], ext="_num",
                                            model=model_)
        res2 = modeling_config.compare_model(data_true=modeling_config.num_data.data_num_raw, data_test=result_BC2,
                                             list_sensors=['T2', 'T2-3', 'Tsin'], ext="_num", model=model_)
        print("Residuals with BC1", res)
        print("Residuals with BC2", res2)

        # Sensivity regarding BC
        df_sens_BC = modeling_config.sens_BC(model_, BC=1, BC_type="num", hcout='hconv_out_ISO6946',
                                             list_sensors=['Tsout', 'T1-2', 'T2', 'T2-3', 'Tsin'],
                                             file="",
                                             folder="")

        # Sensitivity regarding parameters
        df_sens_param = modeling_config.sens_param(model_, BC=1, BC_type="num", hcout='hconv_out_ISO6946',
                                                   list_sensors=['Tsout', 'T1-2', 'T2', 'T2-3', 'Tsin'],
                                                   file="",
                                                   folder="")

        # Plotting.sens_BC_plot_2days(Modeling=modeling_config, df=df_sens_BC,
        #                             sensor_names=['Sensor S$_{out}$', 'Sensor S$_{1-2}$', 'Sensor S$_{2}$',
        #                                           'Sensor S$_{2-3}$', 'Sensor S$_{in}$'],
        #                             list_names=["ab_SW", "hconv_out_ISO6946", "hr_out", "h_in"],
        #                             start_day1=date(2019,2,26), start_day2=date(2019,3,4),
        #                             name_day1='Sunny day', name_day2='Cloudy day', file="2-Sens_BC_2days_BC1", BC=1)
        #
        # Plotting.sens_param_plot_2days(Modeling=modeling_config, df=df_sens_param[0],
        #                             sensor_names=['Sensor S$_{2}$','Sensor S$_{2-3}$', 'Sensor S$_{in}$'],
        #                             list_names=["L13", "L2", "C13", "C2"],
        #                             start_day1=date(2019,2,26), start_day2=date(2019,3,4),
        #                             name_day1='Sunny day', name_day2='Cloudy day', file="2-Sens_param_2days_BC1", BC=1)

        # Sensivity regarding BC
        df_sens_BC_BC2 = modeling_config.sens_BC(model_, BC=2, BC_type="num", hcout='hconv_out_ISO6946',
                                             list_sensors=['T2', 'T2-3', 'Tsin'],
                                             file="",
                                             folder="")
        df_sens_param_BC2 = modeling_config.sens_param(model_, BC=2, BC_type="num", hcout='hconv_out_ISO6946',
                                                   list_sensors=['T2', 'T2-3', 'Tsin'],
                                                   file="",
                                                   folder="")

        Plotting.sens_BC_plot_2days(Modeling=modeling_config, df=df_sens_BC_BC2,
                                    sensor_names=['Sensor S$_{2}$',
                                                  'Sensor S$_{2-3}$', 'Sensor S$_{in}$'],
                                    list_names=["T1-2", "h_in"],
                                    start_day1=date(2019,2,26), start_day2=date(2019,3,4),
                                    name_day1='Sunny day', name_day2='Cloudy day', file="2-Sens_BC_2days_BC2", BC=2)

        Plotting.sens_param_plot_2days(Modeling=modeling_config, df=df_sens_param_BC2[0],
                                    sensor_names=['Sensor S$_{2}$','Sensor S$_{2-3}$', 'Sensor S$_{in}$'],
                                    list_names=["L13", "L2", "C13", "C2"],
                                    start_day1=date(2019,2,26), start_day2=date(2019,3,1),
                                    name_day1='Sunny day', name_day2='01/03/2019', file="2-Sens_param_2days_BC2", BC=2)

    sys.exit()
